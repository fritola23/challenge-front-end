import React, {Component} from 'react';
import facebook_icon from '../assets/facebook_ico.png';
import google_icon from '../assets/google_ico.png';
import twitter_icon from '../assets/twitter_ico.png';
import pinterest_icon from '../assets/pinterest_ico.png';


export default class Footer extends Component {

    constructor(props) {
        super(props)
        this.state = {value: ''};                        
      
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('Um nome foi enviado: ' + this.state.value);
    event.preventDefault();
  }

render(){
        return (
            <div className="footer">
                <div className="content_form">
                    <h1>Keep in touch with us</h1>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Praesent vitae eros eget tellus <br/> tristique bibendum. 
                        Donec rutrum sed sem quis venenatis.
                    </p>

                    <div className="form_contact">
                        <form onSubmit={this.handleSubmit}>
                            <input value={this.state.value} onChange={this.handleChange} type="email" placeholder="Enter your email to update" />                    
                        </form>
                        <button onClick={this.handleSubmit} type="submit">
                            SUBMIT
                        </button>                
                    </div>

                    <div className="social_icons">
                        <img alt="social_icon" src={facebook_icon} />
                        <img alt="social_icon" src={twitter_icon} />
                        <img alt="social_icon" src={google_icon} />
                        <img alt="social_icon" src={pinterest_icon} />                
                    </div>

                    <div className="adress_list">
                        <p>
                            Alameda Santos, 1978 <br />
                            6th floor - Jardim Paulista <br />
                            São Paulo - SP  <br />
                            +55 11 3090 8500
                        </p>

                        <p>
                            London - UK <br />
                            125 Kingsway <br />
                            London WC2B 6NH
                        </p>

                        <p>
                        Lisbon – Portugal <br/>
                        Rua Rodrigues Faria, 103<br/>
                        4th floor<br/>
                        Lisbon – Portugal
                        </p>

                        <p>
                        Curitiba – PR <br/>
                        R. Francisco Rocha, 198<br/>
                        Batel – Curitiba – PR
                        </p>
                        
                        <p>
                        Buenos Aires – Argentina <br/>
                        Esmeralda 950 <br/>
                        Buenos Aires B C1007
                        </p>

                    </div>

                </div>
            </div>
        );
    }
}
