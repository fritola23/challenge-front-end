import React from 'react';
import logo from '../assets/logo.png';

function Header() {
  return (
    <div className="header">
      <div className="menu_nav">
        <img src={logo} alt="logo" />
        <ul>
          <li>Books</li>
          <li>Newsletter</li>
          <li>Address</li>
        </ul>
      </div>
    </div>
  );
}

export default Header;
