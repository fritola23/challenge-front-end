import React from 'react';
import 'font-awesome/css/font-awesome.min.css';

function Introduction() {
  return (
    <div className="introduction">
      <h1 className="title">Pixter Digital Books</h1>
      <p className="subtitle">
        Lorem ipsum dolor sit amet?
        <br />
        consectetur elit, volutpat.
      </p>
      <p className="subtitle_content">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br />
        Praesent vitae eros eget tellus tristique bibendum. Donec <br />
        rutrum sed sem quis venenatis. Proin viverra risus a eros <br />
        volutpat tempor. In quis arcu et eros porta lobortis sit
      </p>
      <div className="icons">
        <i className="fa fa-apple" />
        <i className="fa fa-android" />
        <i className="fa fa-windows" />
      </div>
    </div>
  );
}

export default Introduction;
