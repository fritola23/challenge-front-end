import React, { Component } from 'react';
import './styles/style.css';
import './styles/responsive.css';
import 'font-awesome/css/font-awesome.min.css';

import axios from 'axios'
import Header from './Components/header';
import Footer from './Components/footer';
import Introduction from './Components/introduction';
import ipad from './assets/ipad.png';

export default class App extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      books: [],
      modalOpen: false,
      activeBook: ''
    }
    
    this.list()    
    
  }
    
  list(){
    axios.get('https://www.googleapis.com/books/v1/volumes?q=HARRY%20POTTER')    
    .then(res => this.setState({books : res.data.items}))      
  }

  openModal(book){
    console.log(book)
    this.setState({modalOpen: !this.state.modalOpen})
    this.setState({activeBook: book})
  }
  
  render() {            
    console.log(this.state.activeBook)
    return (
      <div className="App">
        <div className="container">
          <Header />

          <div className="content_introduction">
            <Introduction />
            <img src={ipad} alt="ipad" />
          </div>

          <div className="content">
            <h1>Books</h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
              vitae eros eget tellus <br /> tristique bibendum. Donec rutrum sed
              sem quis venenatis.
            </p>
            
            <div className="books">

              {
                this.state.books.map(book => 
                  !!book.volumeInfo.imageLinks && 
                  <>
                  <button key={book.id} onClick={() => this.openModal(book)} type="button">
                    <img key={book.id} src={book.volumeInfo.imageLinks.thumbnail} alt={book.volumeInfo.title} />
                  </button>                                                                        
                  </>
                )
              }                                           
              
            </div>      


            {!!this.state.modalOpen &&
              <div className="modal_book_active">                
                <div className="close_button">
                  <button onClick={() => this.openModal()}>
                    <i className="fa fa-times" />
                  </button>
                </div>     
                <div className="book_informations">
                  <p>{this.state.activeBook.volumeInfo.title}</p>                
                  <p>Published in {this.state.activeBook.volumeInfo.publishedDate}</p>                
                  <p>{this.state.activeBook.volumeInfo.description}</p>                
                </div>             
            </div>}

          </div>
            
          <Footer />

        </div>
      </div>
    );
  }
}
